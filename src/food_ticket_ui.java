import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class food_ticket_ui {
    private JPanel root;
    private JButton TempuraButton;
    private JButton karaageButton;
    private JButton GyozaButton;
    private JButton UdonButton;
    private JButton YakisobaButton;
    private JButton RamenButton;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel total;
    int price,sokei;

    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0){
            textPane1.setText(textPane1.getText() + food + "\n");
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible.");
            sokei += price;
            total.setText("total:"+sokei+"\n");

        }
    }

    public food_ticket_ui() {
        TempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=100;
                order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=100;
                order("Karaage");
            }
        });
        GyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=100;
                order("Gyoza");
            }
        });
        UdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=100;
                order("Udon");
            }
        });
        YakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=100;
                order("Yakisoba");
            }
        });
        RamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=100;
                order("Ramen");
            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is " + sokei + " yen.");
                    sokei = 0;
                    total.setText("total:"+sokei+"\n");
                    textPane1.setText("");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("e_3_ui");
        frame.setContentPane(new food_ticket_ui().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
